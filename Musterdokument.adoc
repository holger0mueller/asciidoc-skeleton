= <Dokumententitel>
:author_name: <Autor>
:email: <E-Mail-Adresse>
:lang: de
:hyphens: de
:revnumber: <Revision>
:revdate: <Datum>
:stylesdir: style
:stylesheet: boot-paper.css
:pdf-style: style-pdf.yml
:pdf-stylesdir: style
:source-highlighter: highlight.js
:icons: font
:icon-set: far
:imagesdir: img
:sectnums:
:description: <Beschreibung>
:keywords: <Schlüsselwörter mit Leerzeichen getrennt>

////
unterdückt die Nummierung ab hier
////
:sectnums!:

== Ein Abschnitt ohne Nummerierung

Dieses Dokument zeigt, was man mit AsciiDoctor machen kann (zumindest einen kleinen Teil davon).
Der Document-Header hier drüber sollte befüllt werden.
Er muss auf jeden Fall zusammen bleiben und darf nicht durch Leerzeilen unterbrochen werden!

:sectnums:

////
schaltet die Nummierung wieder ein
////

== Einfaches Markup

Das hier ist normaler Text.
Der geht hier weiter.

Das ist ein Absatz.

Das hier ist *fett*, dieses _kursiv_, dieses #hervorgehoben#.

[quote, Donald Knuth]
Beware of bugs in the above code; I have only proved it correct, not tried it.

Das war ein Zitat.

Das hier ist ein https://asciidoctor.org/docs/asciidoc-writers-guide/#mild-punctuation-strong-impact[Link zu mehr Formatierungs-Dingen].

== Listen

AsciiDoctor kann wunderbare Listen.

.Ohne Nummerierung (aber mit Titel)
* ohne Nummerierung
** aber mit Schachtelung
* ohne Nummerierung

.Mit Nummerierung (und Titel)
. mit Nummerierung
.. und Schachtelung

.Mit Nummerierung, eigenem Startwert und Titel
[start=4]
. und sogar
. mit eigenen Startwerten

Wer keinen Listentitel braucht, lässt die Zeile einfach weg.

Wer mehr wissen möchte, schaut https://asciidoctor.org/docs/asciidoc-writers-guide/#lists-lists-lists[hier].


== Code

Inline-Code `System.out.println()` steht in Backticks.

Eine einzelne Codezeile wird mit einem Leerzeichen eingerückt:

 System.out.println("Hello World!");

Ein Code-Block mit Syntax-Highlighting und sogenannten "Callouts", die das Ansprechen der jeweiligen Zeile ermöglichen:

[source,java]
----
public class HelloWorld{                        // <1>

    public static void main(String[] args){     // <2>
        System.out.println("Hello World!");     // <3>
    }
}
----
<1> Klassendeklaration
<2> Methodendeklaration `main()`-Methode
<3> Ausgabe auf der Kosole

[WARNING]
====
Wenn kein Syntax-Highlighting nötig ist, kann die Angabe der Programmiersprache weggelassen werden.
Falls eine Programmiersprache angegeben ist, wird der Code auch auf Vollständigkeit geprüft!

Diese Blöcke hier heißen https://asciidoctor.org/docs/asciidoc-writers-guide/#admonition-blocks[Admonition-Blocks] und es gibt sie in verschiedenen Geschmacksrichtungen (Link angucken).
====

== Bilder

Bilder können aus lokalen Dateien eingebunden werden oder aus dem Netz verlinkt werden:

image::Logo_Proposal.png[Logo-Vorschlag Asciidoctor,75%,75%,align="center"]

Dieses Bild wurde etwas verkleinert und zentiert.
Als Bilderverzeichnis ist `img` im Document-Header eingestellt, d.h. alle Bilder-Pfadangaben erfolgen relativ dazu.

Wer mehr wissen möchte, schaut https://asciidoctor.org/docs/asciidoc-writers-guide/#links-and-images[hier].

