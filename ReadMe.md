Dies hier ist ein Musterprojekt für AsciiDoctor-Dokumente.

Das ist als Java-Projekt vorbereitet, falls es für "Übungsaufgabe mit Musterlösung" benutzt werden soll.
Wer die Java-Funktionalität nicht braucht, tut einfach so, als gäbe es die nicht.

### Vorbereitung der Softwareumgebung

Asciidoctor ist in [Ruby](https://www.ruby-lang.org/de/) geschrieben.
Da das eine interpretierte Sprache ist, muss der Interpreter bzw. die Laufzeitumgebung installiert werden:

1. [Ruby installieren](https://www.ruby-lang.org/de/downloads/)
2. mittels Rubys Paketmanager `gem` in einer beliebigen Shell die nötigen Pakete installieren:
```batch
gem install asciidoctor asciidoctor-pdf text-hyphen
```

Dies installiert die Pakete AsciiDoctor (braucht man), Asciidoctor-PDF (braucht man, um PDFs zu erzeugen) und Text-Hyphen (weil Silbentrennung geil ist).
Diese Pakete werden aus dem Internet heruntergeladen (von rubygems.org) und installiert.

Mit ```asciidoctor-pdf --version``` kann überprüft werden, ob man eine funktionstüchtige Umgebung hat.


### Vorbereitung der IDE IntelliJ IDEA

Mit den passenden Plugins wird [IntelliJ](https://www.jetbrains.com/de-de/idea/) zu einer sehr komfortablen Schreibumgebung für AsciiDoc.

Empfohlene Plugins:

* [AsciiDoc](https://plugins.jetbrains.com/plugin/7391-asciidoc), sorgt für Syntax-Highlighting, Autocompletion und eine HTML-Vorschau
* [Grazie](https://plugins.jetbrains.com/plugin/12175-grazie), ein richtig guter Spell- und Stylechecker, der auch gut Deutsch spricht. Leider kann man im Ausbildungsnetz keine Sprachen nachinstallieren, da muss ich nochmal schauen, warum das so ist. Der läuft als "Inspection", also findet man ihn unter *Analyze* -> *Inspect Code* 
* [Batch Scripts Support](https://plugins.jetbrains.com/plugin/265-batch-scripts-support), da das "Build-Werkzeug" ein Batch-Skript ist.

Das "Build-Skript" sieht so aus:

```batch
@ECHO OFF

for /r %%i in (*.adoc) do asciidoctor-pdf -a pdf-style=style-pdf.yml %%i
```

Es tut nichts anderes, als alle AsciiDoc-Dateien (`*.adoc`) im aktuellen Verzeichnis und allen Unterverzeichnissen zu finden und diese an `asciidoctor-pdf` zu verfüttern.
Wer mag, kann das natürlich auch jedes Mal in IntelliJs Shell eintippen...